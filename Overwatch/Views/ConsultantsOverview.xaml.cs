﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Overwatch.Views;
using Overwatch.ViewModels;

namespace Overwatch
{
    /// <summary>
    /// Interaction logic for ConsultantsOverview.xaml
    /// </summary>
    public partial class ConsultantsOverview : Window
    {
        public ConsultantsOverview( OverviewViewModel viewModel)
        {
            InitializeComponent();
            DataContext = viewModel;
            ////setting up default values for window
            //Owner_Button.Content = viewModel.Owner.CompanyName;
            //Architect_Button.Content = viewModel.Architect.CompanyName;
            //Structure_Button.Content = viewModel.Structural.CompanyName;
            //Mechanical_Button.Content = viewModel.Mechanical.CompanyName;
            //Electrical_Button.Content = viewModel.Electrical.CompanyName;
            //Plumbing_Button.Content = viewModel.Plumbing.CompanyName;
            //Fire_Button.Content = viewModel.FireProtection.CompanyName;
            //Civil_Button.Content = viewModel.Civil.CompanyName;
            //Landscape_Button.Content = viewModel.Landscape.CompanyName;
            //AV_Button.Content = viewModel.AudioVisual.CompanyName;
            //Sustainability_Button.Content = viewModel.Sustainability.CompanyName;
            //Lighting_Button.Content = viewModel.Lighting.CompanyName;
            //Spec_Button.Content = viewModel.Specifications.CompanyName;
            //Vert_Button.Content = viewModel.VerticalCirculation.CompanyName;
            //Energy_Button.Content = viewModel.EnergyModeling.CompanyName;
            //Equip_Button.Content = viewModel.SpecialtyEquipment.CompanyName;
            //Life_Button.Content = viewModel.LifeSafety.CompanyName;
            //Const_Button.Content = viewModel.Construction.CompanyName;
            //Cons1_Button.Content = viewModel.Consultant1.CompanyName;
            //Cons2_Button.Content = viewModel.Consultant2.CompanyName;
        }

       

        
        

        private void Architect_Button_Click(object sender, RoutedEventArgs e)
        {
           
        }

        private void Structure_Button_Click(object sender, RoutedEventArgs e)
        {
            //DetailViewModel structureVM = new DetailViewModel(MainCommand.consultants[1]);
            
            
            ConsultantDetail structureView = new ConsultantDetail(MainCommand.consultants[1]);
            structureView.Show();
        }

        

        private void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
