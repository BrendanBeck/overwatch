﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Services.Client;

using RestSharp;
using Overwatch.Models;

namespace Overwatch
{
    public class APIRequests
    {

        //public T Execute<T>(RestRequest request) where T : new()
        //{
        //    var client = new RestClient();
        //    client.BaseUrl = new Uri("http://dalinet01:8080/api/");
        //    var response = client.Execute<T>(request);
        //    if (response.ErrorException != null)
        //    {
        //        const string message = "Error retrieving response.  Check inner details for more info.";
        //        var exception = new ApplicationException(message, response.ErrorException);
        //        throw exception;
        //    }
        //    return response.Data;

        //}

        //public Proposal GetProposal(int id)
        //{
        //    var request = new RestRequest(Method.GET);
        //    request.Resource = string.Format("Proposal({0})", Convert.ToString(id));
        //    return Execute<Proposal>(request);
        //}



        //######################################
        // Odata requests
        //######################################


        static Uri uri = new Uri("http://dalinet01:8080/api/");
        ODataService.Container container = new ODataService.Container(uri);

        public List<ODataService.Proposal> GetProposals ()
        {
            List<ODataService.Proposal> proposals = container.Proposal.ToList();
            return proposals;
        }

        public ODataService.Proposal GetProposal(int id)
        {
            ODataService.Proposal proposal = container.Proposal.Where(p => p.id == id).FirstOrDefault();
            return proposal;
        }

        public List<ODataService.ConsultantName> GetConsultantsByProject (int proposalId)
        {
            List<ODataService.ProposalConsultantMap> consltMaps = new List<ODataService.ProposalConsultantMap>();
            List<ODataService.ConsultantName> consultants = new List<ODataService.ConsultantName>();
            foreach(ODataService.ProposalConsultantMap p in container.ProposalConsultantMap.Where(p => p.ProposalID == proposalId))
            {
                consltMaps.Add(p);
            }
            if (consltMaps.Count != 0)
            {
                foreach(ODataService.ProposalConsultantMap m in consltMaps)
                {
                    //ConsultantEntry consultant = new ConsultantEntry();
                    //ODataService.ConsultantType type = container.ConsultantType.Where(t => t.id == m.ConsultantTypeID).FirstOrDefault();
                    ODataService.ConsultantName cName = container.ConsultantName.Where(n => n.id == m.ConsultantNameID).FirstOrDefault();
                    //List<ODataService.ConsultantRep> cReps = container.ConsultantRep.Where(r => r.ConsultantNameID == cName.id)
                    consultants.Add(cName);
                }
            }
            return consultants;           
        }

        public ODataService.ConsultantType GetConsultantTypeById(int id)
        {
            ODataService.ConsultantType type = container.ConsultantType.Where(t => t.id == id).FirstOrDefault();
            return type;
        }

        public ODataService.ConsultantType GetConsultantTypeByProposalandID (int proposalID, int consultID)
        {
            List<ODataService.ProposalConsultantMap> consltMaps = new List<ODataService.ProposalConsultantMap>();
            ODataService.ConsultantType myType = new ODataService.ConsultantType();
            foreach (ODataService.ProposalConsultantMap p in container.ProposalConsultantMap.Where(p => p.ProposalID == proposalID))
            {
                consltMaps.Add(p);
            }
            foreach (ODataService.ProposalConsultantMap p in consltMaps)
            {
                if (p.ConsultantNameID == consultID)
                {
                    myType = container.ConsultantType.Where(t => t.id == p.ConsultantTypeID).FirstOrDefault();
                    
                }
                else
                {
                }
            }
            return myType;
        }

        public List<ODataService.ConsultantRep> GetRepsByCnsltNameID(int id)
        {
            List<ODataService.ConsultantRep> reps = container.ConsultantRep.Where(r => r.ConsultantNameID == id).ToList();
            return reps;
        }



    }


}
