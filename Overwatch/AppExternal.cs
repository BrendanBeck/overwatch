﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Media.Imaging;

using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Architecture;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
using Autodesk.Revit.ApplicationServices;
using Autodesk.Revit.Attributes;


namespace Overwatch
{
    class AppExternal : IExternalApplication
    {
        static string _path = typeof(AppExternal).Assembly.Location;
        


        public Result OnStartup(UIControlledApplication app)
        {

            RibbonPanel overWatch = app.CreateRibbonPanel("Beck", "OverWatch");
            PushButtonData buttonData = new PushButtonData("myButton", "Update Consultants", _path, "Overwatch.MainCommand");
            
            Uri uriImage = new Uri("////Atlfs//public//BNichols//Coding//Overwatch//images//obey32.jpg");
            BitmapImage largeImage = new BitmapImage(uriImage);

            buttonData.LargeImage = largeImage;

            RibbonItem myButton = overWatch.AddItem(buttonData);
            


            return Result.Succeeded;
        }








        public Result OnShutdown(UIControlledApplication app)
        {
            return Result.Succeeded;
        }
    }
}
