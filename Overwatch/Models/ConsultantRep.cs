﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Overwatch.Models
{
    public class ConsultantRep
    {
        public int ConsultantNameID { get; set; }
        public string NameFirst { get; set; }
        public string NameMiddle { get; set; }
        public string NameLast { get; set; }
        public string NamePreferrred { get; set; }
        public string NamePrefix { get; set; }
        public string NameSuffix { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int SelectedContactLevel { get; set; }
    }
}
