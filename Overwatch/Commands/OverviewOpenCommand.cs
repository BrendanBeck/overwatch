﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Overwatch.ViewModels;
using Overwatch.Models;

namespace Overwatch.Commands
{
    public class OverviewOpenCommand : ICommand
    {
        public OverviewOpenCommand(OverviewViewModel viewModel)
        {
            this.ViewModel = viewModel;
        }
        private OverviewViewModel ViewModel;

        public event EventHandler CanExecuteChanged;
        
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            if (parameter != null)
            {
                this.ViewModel.OpenDetailView(parameter as ConsultantEntry);
            }
            else
            {
                ConsultantEntry c = new ConsultantEntry();
                this.ViewModel.OpenDetailView(c);
            }
        }
    }
}
