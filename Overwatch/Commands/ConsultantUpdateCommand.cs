﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Overwatch.ViewModels;
using System.Windows.Input;
using System.Windows.Controls;

namespace Overwatch.Commands
{
    internal class ConsultantUpdateCommand : ICommand
    {
        public ConsultantUpdateCommand(DetailViewModel viewModel)
        {
            ViewModel = viewModel;
        }
        private DetailViewModel ViewModel;

        event EventHandler ICommand.CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }

            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        bool ICommand.CanExecute(object parameter)
        {
            return ViewModel.CanUpdate;
        }

        void ICommand.Execute(object parameter)
        {

            ViewModel.ChangeSelection(1);
        }
    }
}
