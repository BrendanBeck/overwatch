﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Architecture;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
using Autodesk.Revit.ApplicationServices;
using Autodesk.Revit.Attributes;

using Overwatch.Models;
using Overwatch.ViewModels;
using Overwatch.Views;

namespace Overwatch
{
    
    [TransactionAttribute(TransactionMode.Manual)]
    [RegenerationAttribute(RegenerationOption.Manual)]
    public class MainCommand : IExternalCommand
    {
        public Result Execute(
          ExternalCommandData commandData,
          ref string message,
          ElementSet elements)
        {
            UIApplication uiApp = commandData.Application;
            Document doc = uiApp.ActiveUIDocument.Document;

            OverviewViewModel MainOverviewViewModel = new OverviewViewModel();

            List<Proposal> proposals = GetProposals();
            MainOverviewViewModel.Proposals = proposals;
            ConsultantsOverview mainView = new ConsultantsOverview(MainOverviewViewModel);
            //ProjectFiller(1);
            mainView.Show();
            return Result.Succeeded;

        }

        public List<Proposal> GetProposals()
        {
            APIRequests apiRequest = new APIRequests();
            List<Proposal> cleanedProposals = new List<Proposal>();
            List<ODataService.Proposal> proposals = apiRequest.GetProposals();
            foreach(ODataService.Proposal p in proposals)
            {
                Proposal thisProposal = new Proposal();
                thisProposal.CreatedDate = p.CreatedDate;
                thisProposal.Id = p.id;
                thisProposal.ProjectName = p.ProjectName;
                cleanedProposals.Add(thisProposal);
            }
            return cleanedProposals;
        }
    

        public static void ProposalFiller(int id)
        {

            APIRequests apiRequest = new APIRequests();

            try
            {
                ODataService.Proposal myProposal = new ODataService.Proposal();
                myProposal = apiRequest.GetProposal(id);
                if (myProposal.id != 0)
                {
                    ConsultantFiller(myProposal);
                }
                else
                {
                    const string message = "Job not found. Please check CMIC Job number";
                    var exception = new ApplicationException(message);
                    throw exception;
                }
            }
            catch { }
        }


        public static void ConsultantFiller (ODataService.Proposal proposal)
        {
            APIRequests apiRequest = new APIRequests();
            List<ODataService.ConsultantName> consultantNames = apiRequest.GetConsultantsByProject(proposal.id);
            foreach (ODataService.ConsultantName c in consultantNames)
            {
                ConsultantEntry entry = new ConsultantEntry();
                entry.ConsultantNameID = c.id;
                if (c.ConsultantTypeID != null)
                {
                    entry.ConsultantTypeID = (int)c.ConsultantTypeID;
                }
                entry.CompanyName = c.CompanyName;
                entry.Address1 = c.Address1;
                entry.Address2 = c.Address2;
                entry.City = c.City;
                entry.State = c.State;
                entry.Post = c.Post;
                entry.Country = c.Country;
                entry.Phone = c.Phone;
                entry.Fax = c.Fax;
                entry.Email = c.Email;
                consultants.Add(entry);
            }
            foreach (ConsultantEntry entry in consultants)
            {
                entry.ConsultantType = apiRequest.GetConsultantTypeByProposalandID(proposal.id,entry.ConsultantNameID).ConsultantType1;               
            }
            foreach (ConsultantEntry entry in consultants)
            {
                entry.ConsultantReps = new List<ConsultantRep>();
                List<ODataService.ConsultantRep> reps = apiRequest.GetRepsByCnsltNameID(entry.ConsultantNameID);
                foreach (ODataService.ConsultantRep r in reps)
                {
                    if (r.id != 0)
                    {
                        ConsultantRep myRep = new ConsultantRep();
                        myRep.ConsultantNameID = entry.ConsultantNameID;
                        myRep.NameFirst = r.NameFirst;
                        myRep.NameLast = r.NameLast;
                        myRep.NamePreferrred = r.NamePreferred;
                        myRep.NamePrefix = r.NamePrefix;
                        myRep.NameSuffix = r.NameSuffix;
                        myRep.Email = r.Email;
                        myRep.Phone = r.Phone;
                        entry.ConsultantReps.Add(myRep);
                    }
                }
            }

        }


        //public DetailViewModel makeDetailViewModel (string buttonContent)
        //{
        //    DetailViewModel detailViewModel = new DetailViewModel();
        //    foreach (ConsultantEntry e in consultants)
        //    {
        //        if (e.SelectedRole == buttonContent)
        //        {
        //            detailViewModel.selectedConsultant = e;
        //            detailViewModel.rep1 = e.ConsultantReps[0];                    
        //        }
        //    }
        //    return detailViewModel;
        //}

        public OverviewViewModel makeDefaultViewModel ()
        {
            OverviewViewModel defaultViewModel = new OverviewViewModel();
            defaultViewModel.Owner = new ConsultantEntry();
            defaultViewModel.Owner.CompanyName = "Owner";
            defaultViewModel.Architect = new ConsultantEntry();
            defaultViewModel.Architect.CompanyName = "Architect";
            defaultViewModel.Structural = new ConsultantEntry();
            defaultViewModel.Structural.CompanyName = "Structural";
            defaultViewModel.Mechanical = new ConsultantEntry();
            defaultViewModel.Mechanical.CompanyName = "Mechanical";
            defaultViewModel.Electrical = new ConsultantEntry();
            defaultViewModel.Electrical.CompanyName = "Electrical";
            defaultViewModel.Plumbing = new ConsultantEntry();
            defaultViewModel.Plumbing.CompanyName = "Plumbing";
            defaultViewModel.FireProtection = new ConsultantEntry();
            defaultViewModel.FireProtection.CompanyName = "Fire Protection";
            defaultViewModel.Civil = new ConsultantEntry();
            defaultViewModel.Civil.CompanyName = "Civil";
            defaultViewModel.Landscape = new ConsultantEntry();
            defaultViewModel.Landscape.CompanyName = "Landscape";
            defaultViewModel.AudioVisual = new ConsultantEntry();
            defaultViewModel.AudioVisual.CompanyName = "Audio/Visual";
            defaultViewModel.Sustainability = new ConsultantEntry();
            defaultViewModel.Sustainability.CompanyName = "Sustainability";
            defaultViewModel.Lighting = new ConsultantEntry();
            defaultViewModel.Lighting.CompanyName = "Lighting";
            defaultViewModel.Specifications = new ConsultantEntry();
            defaultViewModel.Specifications.CompanyName = "Specifications";
            defaultViewModel.VerticalCirculation = new ConsultantEntry();
            defaultViewModel.VerticalCirculation.CompanyName = "Vertical Circulation";
            defaultViewModel.EnergyModeling = new ConsultantEntry();
            defaultViewModel.EnergyModeling.CompanyName = "Energy Modeling";
            defaultViewModel.SpecialtyEquipment = new ConsultantEntry();
            defaultViewModel.SpecialtyEquipment.CompanyName = "Specialty Equipment";
            defaultViewModel.LifeSafety = new ConsultantEntry();
            defaultViewModel.LifeSafety.CompanyName = "Life Safety";
            defaultViewModel.Construction = new ConsultantEntry();
            defaultViewModel.Construction.CompanyName = "Construction";
            defaultViewModel.Consultant1 = new ConsultantEntry();
            defaultViewModel.Consultant1.CompanyName = "Consultant";
            defaultViewModel.Consultant2 = new ConsultantEntry();
            defaultViewModel.Consultant2.CompanyName = "Consultant";
            return defaultViewModel;
        }


        //    Architect_Button.Content = "Architect";
        //    Structure_Button.Content = "Structural";
        //    Mechanical_Button.Content = "Mechanical";
        //    Electrical_Button.Content = "Electrical";
        //    Plumbing_Button.Content = "Plumbing";
        //    Fire_Button.Content = "Fire Protection";
        //    Civil_Button.Content = "Civil";
        //    Landscape_Button.Content = "Landscape";
        //    AV_Button.Content = "Audio/Visual";
        //    Sustainability_Button.Content = "Sustainability";
        //    Lighting_Button.Content = "Lighting";
        //    Spec_Button.Content = "Specifications";
        //    Vert_Button.Content = "Vertical Circulation";
        //    Energy_Button.Content = "Energy Modeling";
        //    Equip_Button.Content = "Specialty Equipment";
        //    Life_Button.Content = "Life Safety";
        //    Const_Button.Content = "Construction";
        //    Cons1_Button.Content = "Consultant";
        //    Cons2_Button.Content = "Consultant";


        public static List<ConsultantEntry> consultants = new List<ConsultantEntry>();
        public static OverviewViewModel MainOverviewViewModel { get; set; }
    }
    
}
