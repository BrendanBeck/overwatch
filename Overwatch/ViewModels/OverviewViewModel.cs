﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Overwatch.Models;
using Overwatch.Commands;
using Overwatch.ViewModels;
using Overwatch.Views;
using System.ComponentModel;
using System.Windows.Input;

namespace Overwatch.ViewModels
{
    public class OverviewViewModel : INotifyPropertyChanged
    {
        public OverviewViewModel()
        {
            OverviewOpenMethod = new OverviewOpenCommand(this);
            GetConsultants = new GetConsultantsCommand(this);
        }

        public void OpenDetail()
        {
            
        }
        public GetConsultantsCommand GetConsultants { get; set; }

        public OverviewOpenCommand OverviewOpenMethod { get; set; }
    

        public void GetConsultantsMethod()
        {
            //need to also fire off get owner command
            MainCommand.ProposalFiller(selectedProposal.Id);
            BestFitConsultants();
        }

        public void BestFitConsultants()
        {
            foreach(ConsultantEntry c in MainCommand.consultants)
            {
                
                if (c.ConsultantType == "Structural")
                {
                    Structural = c;
                }
                else if(c.ConsultantType == "Mechanical")
                {
                    Mechanical = c;
                }
                else if(c.ConsultantType == "Electrical")
                {
                    Electrical = c;
                }
                else if(c.ConsultantType == "Plumbing")
                {
                    Plumbing = c;
                }
                else if(c.ConsultantType == "Civil Engineering")
                {
                    Civil = c;
                }
                else if(c.ConsultantType == "Landscape")
                {
                    Landscape = c;
                }
                else if(c.ConsultantType == "Technology/AV Consultant")
                {
                    AudioVisual = c;
                }
                else if(c.ConsultantType == "LEED")
                {
                    Sustainability = c;
                }
                else if(c.ConsultantType == "Lighting Design")
                {
                    Lighting = c;
                }
                else if(c.ConsultantType == "Vertical Transportation")
                {
                    VerticalCirculation = c;
                }
                else if(c.ConsultantType == "Green Building")
                {
                    EnergyModeling = c;
                }
                else if(c.ConsultantType == "Life Safety")
                {
                    LifeSafety = c;
                }
                else if(Consultant1 == null)
                {
                    Consultant1 = c;
                }
                else if (Consultant2 == null)
                {
                    Consultant2 = c;
                }
            }

        }

        public bool CanUpdate { get; internal set; }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        internal void OpenDetailView(ConsultantEntry consultantEntry)
        {            
            ConsultantDetail detailView = new ConsultantDetail(consultantEntry);
            detailView.Show();
        }
        public Proposal SelectedProposal
        {
            get
            {
                return selectedProposal;
            }
            set
            {
                this.selectedProposal = value;
                this.OnPropertyChanged("SelectedProposal");
            }
        }
        public List<Proposal> Proposals
        {
            get
            {
                return proposals;
            }
            set {
                proposals = value;
            }
            //{
            //    this.proposals = value;
            //    this.OnPropertyChanged("Proposals");
            //}
        }
        public ConsultantEntry Owner
        {
            get
            {
                return owner;
            }
            set
            {
                this.owner = value;
                this.OnPropertyChanged("Owner");
            }
        } 
        public ConsultantEntry Architect
        {
            get
            {
                return architect;
            }
            set
            {
                this.architect = value;
                this.OnPropertyChanged("Architect");
            }
        }
        public ConsultantEntry Structural
        {
            get
            {
                return structural;
            }
            set
            {
                this.structural = value;
                this.OnPropertyChanged("Structural");
            }
        }
        public ConsultantEntry Mechanical
        {
            get
            {
                return mechanical;
            }
            set
            {
                this.mechanical = value;
                this.OnPropertyChanged("Mechanical");
            }
        }
        public ConsultantEntry Electrical
        {
            get
            {
                return electrical;
            }
            set
            {
                this.electrical = value;
                this.OnPropertyChanged("Electrical");
            }
        }
        public ConsultantEntry Plumbing
        {
            get
            {
                return plumbing;
            }
            set
            {
                this.plumbing = value;
                this.OnPropertyChanged("Plumbing");
            }
        }
        public ConsultantEntry FireProtection
        {
            get
            {
                return fireProtection;
            }
            set
            {
                this.fireProtection = value;
                this.OnPropertyChanged("FireProtection");
            }
        }
        public ConsultantEntry Civil
        {
            get
            {
                return civil;
            }
            set
            {
                this.civil = value;
                this.OnPropertyChanged("Civil");
            }
        }
        public ConsultantEntry Landscape
        {
            get
            {
                return landscape;
            }
            set
            {
                this.landscape = value;
                this.OnPropertyChanged("Landscape");
            }
        }
        public ConsultantEntry AudioVisual
        {
            get
            {
                return audioVisual;
            }
            set
            {
                this.audioVisual = value;
                this.OnPropertyChanged("AudioVisual");
            }
        }
        public ConsultantEntry Sustainability
        {
            get
            {
                return sustainability;
            }
            set
            {
                this.sustainability = value;
                this.OnPropertyChanged("Sustainability");
            }
        }
        public ConsultantEntry Lighting
        {
            get
            {
                return lighting;
            }
            set
            {
                this.lighting = value;
                this.OnPropertyChanged("Lighting");
            }
        }
        public ConsultantEntry Specifications
        {
            get
            {
                return specifications;
            }
            set
            {
                this.specifications = value;
                this.OnPropertyChanged("Specifications");
            }
        }
        public ConsultantEntry VerticalCirculation
        {
            get
            {
                return verticalCirculation;
            }
            set
            {
                this.verticalCirculation = value;
                this.OnPropertyChanged("VerticalCirculation");
            }
        }
        public ConsultantEntry EnergyModeling
        {
            get
            {
                return energyModeling;
            }
            set
            {
                this.energyModeling = value;
                this.OnPropertyChanged("EnergyModeling");
            }
        }
        public ConsultantEntry SpecialtyEquipment
        {
            get
            {
                return specialtyEquipment;
            }
            set
            {
                this.specialtyEquipment = value;
                this.OnPropertyChanged("SpecialtyEquipment");
            }
        } 
        public ConsultantEntry LifeSafety
        {
            get
            {
                return lifeSafety;
            }
            set
            {
                this.lifeSafety = value;
                this.OnPropertyChanged("LifeSafety");
            }
        }
        public ConsultantEntry Construction
        {
            get
            {
                return construction;
            }
            set
            {
                this.construction = value;
                this.OnPropertyChanged("Construction");
            }
        }
        public ConsultantEntry Consultant1
        {
            get
            {
                return consultant1;
            }
            set
            {
                this.consultant1 = value;
                this.OnPropertyChanged("Consultant1");
            }
        }
        public ConsultantEntry Consultant2
        {
            get
            {
                return consultant2;
            }
            set
            {
                this.consultant2 = value;
                this.OnPropertyChanged("Consultant2");
            }
        }

        private Proposal selectedProposal { get; set; }
        private List<Proposal> proposals { get; set; }
        private ConsultantEntry owner { get; set; } //may need seperate class for this later
        private ConsultantEntry architect { get; set; }
        private ConsultantEntry structural { get; set; }
        private ConsultantEntry mechanical { get; set; }
        private ConsultantEntry electrical { get; set; }
        private ConsultantEntry plumbing { get; set; }
        private ConsultantEntry fireProtection { get; set; }
        private ConsultantEntry civil { get; set; }
        private ConsultantEntry landscape { get; set; }
        private ConsultantEntry audioVisual { get; set; }
        private ConsultantEntry sustainability { get; set; }
        private ConsultantEntry lighting { get; set; }
        private ConsultantEntry specifications { get; set; }
        private ConsultantEntry verticalCirculation { get; set; }
        private ConsultantEntry energyModeling { get; set; }
        private ConsultantEntry specialtyEquipment { get; set; }
        private ConsultantEntry lifeSafety { get; set; }
        private ConsultantEntry construction { get; set; }
        private ConsultantEntry consultant1 { get; set; }
        private ConsultantEntry consultant2 { get; set; }
        
    }
}
