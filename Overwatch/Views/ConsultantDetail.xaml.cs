﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Overwatch.Models;
using Overwatch.ViewModels;

namespace Overwatch.Views
{
    /// <summary>
    /// Interaction logic for ConsultantDetail.xaml
    /// </summary>
    public partial class ConsultantDetail : Window
    {
        public ConsultantDetail(ConsultantEntry consult)
        {
            InitializeComponent();            
            DetailViewModel dvm = new DetailViewModel(consult);
            DataContext = dvm;

        }

        private void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //I really wish I had figured out the sexy MVVM way of doing this
        }


    }
}
