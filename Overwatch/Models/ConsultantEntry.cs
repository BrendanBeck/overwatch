﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Overwatch.Models
{
    public class ConsultantEntry :INotifyPropertyChanged
    {
        private int consultantNameID { get; set; }
        private int consultantTypeID { get; set; }
        private string consultantType { get; set; }
        private string companyName { get; set; }
        private string address1 { get; set; }
        private string address2 { get; set; }
        private string city { get; set; }
        private string state { get; set; }
        private string post { get; set; }
        private string country { get; set; }
        private string phone { get; set; }
        private string fax { get; set; }
        private string email { get; set; }
        private string selectedRole { get; set; }
        private  List<ConsultantRep> consultantReps { get; set; }


       

        public int ConsultantNameID
        {
            get
            {
                return consultantNameID;
            }
            set
            {
                consultantNameID = value;
                OnPropertyChanged("ConsultantNameID");
            }
        }
        public int ConsultantTypeID
        {
            get
            {
                return consultantTypeID;
            }
            set
            {
                consultantTypeID = value;
                OnPropertyChanged("ConsultantTypeID");
            }
        }
        public string ConsultantType
        {
            get
            {
                return consultantType;
            }
            set
            {
                consultantType = value;
                OnPropertyChanged("ConsultantType");
            }
        }
        public string CompanyName
        {
            get
            {
                return companyName;
            }
            set
            {
                companyName = value;
                OnPropertyChanged("CompanyName");
            }
        }
        public string Address1
        {
            get
            {
                return address1;
            }
            set
            {
                address1 = value;
                OnPropertyChanged("Address1");
            }
        }
        public string Address2
        {
            get
            {
                return address2;
            }
            set
            {
                address2 = value;
                OnPropertyChanged("Address2");
            }
        }
        public string City
        {
            get
            {
                if (city == null)
                {
                    return "";
                }
                else
                {
                    return city;
                }
            }
            set
            {
                city = value;
                OnPropertyChanged("City");
            }
        }
        public string State
        {
            get
            {
                if (state == null)
                {
                    return "";
                }
                else
                {
                    return state;
                }
            }
            set
            {
                state = value;
                OnPropertyChanged("State");
            }
        }
        public string Post
        {
            get
            {
                if (post == null)
                {
                    return "";
                }
                else
                {
                    return post;
                }
            }
            set
            {
                post = value;
                OnPropertyChanged("Post");
            }
        }
        public string Country
        {
            get
            {
                if (country == null)
                {
                    return "";
                }
                else
                {
                    return country;
                }
            }
            set
            {
                country = value;
                OnPropertyChanged("Country");
            }
        }
        public string Phone
        {
            get
            {
                if (phone == null)
                {
                    return "";
                }
                else
                {
                    return phone;
                }
            }
            set
            {
                phone = value;
                OnPropertyChanged("Phone");
            }
        }
        public string Fax
        {
            get
            {
                return fax;
            }
            set
            {
                fax = value;
                OnPropertyChanged("Fax");
            }
        }
        public string Email
        {
            get
            {
                return email;
            }
            set
            {
                email = value;
                OnPropertyChanged("Email");
            }
        }
        public string SelectedRole
        {
            get
            {
                return selectedRole;
            }
            set
            {
                selectedRole = value;
                OnPropertyChanged("SelectedRole");
            }
        }
        public List<ConsultantRep> ConsultantReps
        {
            get
            {
                return consultantReps;
            }
            set
            {
                consultantReps = value;
                OnPropertyChanged("ConsultantReps");
            }
        }








        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
