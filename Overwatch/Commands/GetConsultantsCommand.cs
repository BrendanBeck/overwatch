﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Overwatch.ViewModels;

namespace Overwatch.Commands
{
    public class GetConsultantsCommand : ICommand
    {
        public OverviewViewModel ViewModel;

        public GetConsultantsCommand(OverviewViewModel viewModel)
        {
            this.ViewModel = viewModel;
        }
        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            this.ViewModel.GetConsultantsMethod();
        }
    }
}
