﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Overwatch.Models;
using Overwatch.Commands;
using System.Windows.Input;
using System.ComponentModel;
using System.Windows.Controls;

namespace Overwatch.ViewModels
{
    public class DetailViewModel : INotifyPropertyChanged
    {
        public DetailViewModel (ConsultantEntry consult)
        {
            selectedConsultant = consult;
            if (consult.ConsultantReps.Count >= 1)
            {
                rep1 = consult.ConsultantReps[0];
            }
            if (consult.ConsultantReps.Count >= 2)
            {
                rep2 = consult.ConsultantReps[1];
            }
            UpdateCommand = new ConsultantUpdateCommand(this);
            consultantTitles = MainCommand.consultants;      
        }

        public ICommand UpdateCommand
        {
            get;
            private set;          
        }

        public void ChangeSelection(int i)
        {
            SelectedConsultant = MainCommand.consultants[3];
            
        }

        public bool CanUpdate = true;

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public void SaveChanges ()
        {
            SelectedConsultant = MainCommand.consultants[0];
        }             
                
        //############################################
        // public fields
        //############################################  

        public ConsultantEntry SelectedConsultant
        {
            get
            {
                return selectedConsultant;
            }
            set
            {
                this.selectedConsultant = value;
                this.OnPropertyChanged("SelectedConsultant");
            }
        }

        public ConsultantRep Rep1
        {
            get
            {
                return rep1;
            }
        }
        public ConsultantRep Rep2
        {
            get
            {
                return rep2;
            }
        }
        public string ConsultantAddress3
        {
            get
            {
                return consultantAddress3;
            }
            set
            {
                consultantAddress3 = value;
                OnPropertyChanged("ConsultantAddress3");
            }
        }
        public List<ConsultantEntry> Titles
        {
            get
            {
                return consultantTitles;
            }
            set { }

        }
        public List<string> RepNames
        {
            get
            {
                return repNames;
            }
        }

        //#################################################
        // private fields
        //#################################################

        private ConsultantEntry selectedConsultant { get; set; }
        private ConsultantRep rep1 { get; set; }
        private ConsultantRep rep2 { get; set; }

        private string consultantAddress3
        {
            get
            {
                return selectedConsultant.City + " " + selectedConsultant.State + " " + selectedConsultant.Country + " " + selectedConsultant.Post;
            }
            set { }
        }

        private List<ConsultantEntry> consultantTitles { get; set; }     

        private List<string> repNames
        {
            get
            {
                List<string> reps = new List<string>();
                foreach (ConsultantRep r in selectedConsultant.ConsultantReps)
                {
                    if (r.ConsultantNameID != rep1.ConsultantNameID && r.ConsultantNameID != rep2.ConsultantNameID)
                    {
                        reps.Add(r.NameFirst + " " + r.NameLast);
                    }
                }
                return reps;
            }
        }

        
    }
}
